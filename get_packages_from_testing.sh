#!/bin/sh

# Colors
RED='\033[0;31m'
LRED="\033[1;31m"
BLUE="\033[0;34m"
LBLUE="\033[1;34m"
GREEN="\033[0;32m"
LGREEN="\033[1;32m"
YELLOW="\033[1;33m"
CYAN="\033[0;36m"
LCYAN="\033[1;36m"
PURPLE="\033[0;35m"
LPURPLE="\033[1;35m"
BWHITE="\e[1m"
NC='\033[0m' # No Color

BINDIR="/usr/bin"
WORKINGDIR="deb"
CACHEDIR="var/cache/apt/archives"
CHANNEL="testing"

if [ ! -f  ${BINDIR}/apt ];then
  echo -e "${LRED}apt was not found in: ${BINDIR}${NC}\n"
  exit 1
fi


if [ $# -eq 0 ];then
    echo -e "${LRED}No package names supplied${NC}\n"
fi

if [ -z "${1}" ];then
  echo -e "${LRED}${0}: Argument string is empty...${NC}\n"
  exit 1
fi

# Clean up existing archives
sudo rm -rf ${CACHEDIR}/*

# Temporarily enable Debian testing and download packages to archives
sudo su -c "echo -e 'deb http://http.debian.net/debian ${CHANNEL} main' > /etc/apt/sources.list.d/testing.list"
sudo apt update
sudo apt -d install "${1}"
sudo rm /etc/apt/sources.list.d/testing.list
sudo apt update

# Copy packages from archives to working directory
mkdir -p ${WORKINGDIR}/
cp -r ${CACHEDIR}/* ${WORKINGDIR}/
